import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
// Controllers :
import HomePage from '../../Component/Routes/Home'

export default function RouteService() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route exact path="/">
                        <HomePage />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}