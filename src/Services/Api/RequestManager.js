import {useEffect, useState} from "react";

export default function RequestService(url) {
    const [Data, setData] = useState(null);
    const [Error, setError] = useState(null);

    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => setData(result),
                (error) => setError(error)
            )
    })

    return Data ? Data : Error;
}