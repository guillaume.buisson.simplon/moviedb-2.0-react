import './App.css';
import RouteService from "./Services/Routes/RouterManager";

function App() {
  return (
    <div className="App">
      <header className="App-header">
            <RouteService />
      </header>
    </div>
  );
}

export default App;
